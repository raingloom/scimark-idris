import Data.Vect
import Data.PosNat
import Data.Bits

{- for acme
win idris --nocolor -p contrib $%
-}

-- must be odd
initSeed : Bits32
initSeed = 101009

band : Bits32 -> Bits32 -> Bits32
band = Data.Bits.and' {n=2}

sar : Bits32 -> Bits32 -> Bits32
sar = Data.Bits.shiftRightArithmetic' {n=32}

RandState : Type
RandState = (Vect 16 Bits32, Bits32, Bits32)

randState0 : RandState
randState0 = (vseed, 16, 11) where
	f : Bits32 -> Bits32
	f x = band (x * 9069) 0x7fffffff
	g : Vect n Bits32 -> Vect (S n) Bits32
	g Nil = [0xdead] -- FIXME this branch should be eliminated
	g (x::xs) = [f x, x] ++ xs
	gen : (n : Nat) -> Vect n Bits32
	gen Z = Nil
	gen (S Z) = [initSeed]
	gen (S n) = g $ gen n
	vseed = reverse $ gen 16


randStep : RandState -> (RandState, Bits32)
randStep (m, j, i) ?= ((m', i', j'), r) where
	f : Bits32 -> Bits32
	f a = band (a + 1) (sar (a `prim__subB32` 16) 31)
	i' : Bits32
	i' = f i
	j' : Bits32
	j' = f j
	k : Bits32
	k = band ((index (fromInteger $ bitsToInt' {n=32} i) m) `prim__subB32` (index (fromInteger $ bitsToInt' {n=32} j) m)) 0x7fffffff
	m' : Vect 16 Bits32
	m' = replaceAt (fromInteger $ bitsToInt' {n=32} j') k m -- FIXME: this should be j' but: No such variable Main.randStep, j'
	r = k * 1.0 / 2147483647.0
